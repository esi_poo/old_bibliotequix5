package com.everis.biblioteca;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

/**
 * Software de gesti�n de bibliotecas
 * @author everis
 */
public class MainBiblioteca {

	public static void listarusuarios() {
		Usuario us = new Usuario("we", "wa", "wo", "wi", "wu", "woe", "wio", 3,"email");
		Usuario us1 = new Usuario("we", "wa", "wo", "wi", "wu", "woe", "wio",2,"email");
		Usuario us2 = new Usuario("we", "wa", "wo", "wi", "wu", "woe", "wio", 1,"email");
		System.out.println(us.toString());
		System.out.println(us1.toString());
		System.out.println(us2.toString());
	}
	
	public static void listarAutores() {
		Author autor = new Author("1", "John", "Romantic",
				"Tragedy", "10/12/1854", "DreamLand",
				"Romantic Writer");
		Author autor1 = new Author("2", "Leiar", "Block",
				"Kind", "10/12/1854", "DreamLand",
				"Realistic Writer");
		System.out.println(autor.toString());
		System.out.println(autor1.toString());
	}

	public static void listarEditorial() {
		int []telefono = {0,1,2,3,4,5,6,7,8};
		Editorial edit = new Editorial(99, "Editorial Everiensis",
				"Lorca",telefono,
				"Salva");
		Editorial edit2 = new Editorial(89, "Editorial Everiensis2",
				"Lorca2",telefono,
				"Salva2");
		System.out.println(edit.toString());
		System.out.println(edit2.toString());

	}

	public static void mostrarFechaHoraActual() {
		Date date = new Date();
		DateFormat hourDateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
		System.out.println("Hora y Fecha: " + hourDateFormat.format(date));
	}


	public static void main(String[] args) {
		
	

	
	
		Biblioteca bib = new Biblioteca(1, "Biblioteca everis Murcia", 1/*"Murcia"*/, 1234567890, "HPC Murcia", 1);
		
		
//		//Fechas:
//		Date f1 = new Date();
//		Date f2 = new Date(); 
//		try {
//			SimpleDateFormat dateformat = new SimpleDateFormat("dd/M/yyyy");
//			f1 =  dateformat.parse("01/01/2015");
//			f2 =  dateformat.parse("01/12/2015");
//		}
//		catch (ParseException e) {
//			System.out.println("Problema con la conversi�n de fechas: " + e.getMessage());
//			System.exit(-1);
//		}
 

		Scanner myScanner = new Scanner(System.in);
		
		int opcion = 0; 
		
		do {
			//MEN�:
			System.out.println(" # ############################################ #");
			System.out.println(" #                  BibliotequIX:               #");
			System.out.println(" # ############################################ #");
			System.out.println(" #  Seleccione una opci�n:                      #");
			System.out.println(" #  1. Listar libros en estado disponible       #");
			System.out.println(" #  2. Editorial                                #");
			System.out.println(" #  3. Listar Usuarios                               #");
			System.out.println(" #  4. Listar Autores                                #");
			System.out.println(" #  5. Fecha/Hora Actual                                #");
			System.out.println(" #  9. Salir                                    #");
			System.out.println(" # ############################################ #");
						
			opcion = myScanner.nextInt();
			
			switch (opcion) {
				case 1: //Listar libros disponibles:
						bib.ListarLibro3(2); 
						System.out.println("");
						break;
				case 2: //Listar Editorial:
					listarEditorial();
					System.out.println("");
					break;
				case 3: //Listar Usuarios:
					listarusuarios();
					System.out.println("");
					break;
				case 4: //Listar Autores:
					listarAutores();
					System.out.println("");
					break;
				case 5: //Fecha/Hora Actual:
					mostrarFechaHoraActual();
					System.out.println("");
					break;
				default: 
						System.out.println(" #  --> Opci�n incorrecta, seleccione otra opci�n:");
						System.out.println("");
			}
		}
		while (opcion != 9);
		
		
		//Mensaje de FIN:
		System.out.println(" # ############################################ #");
		System.out.println(" #              FIN    BibliotequIX:            #");
		System.out.println(" # ############################################ #");
		
		//Cerrar elementos abiertos
		try {
			myScanner.close();
		} catch(Exception ex) {
			System.out.println(" # Error al cerrar 'myScanner': " + ex.getMessage());
			System.exit(-2);
		}
	}

}
